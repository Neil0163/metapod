
Helm is a package manager for Kubernetes.
it can do the following:
- Manage the lifecycle of Kubernetes applications
- Install, upgrade, and uninstall applications on Kubernetes clusters
- Manage configuration and deployment settings for applications
- Create reusable templates for deploying applications
- Manage releases and rollbacks of application versions
- Collaborate and share application configurations with others
- Integrate with continuous integration and continuous deployment (CI/CD) pipelines

