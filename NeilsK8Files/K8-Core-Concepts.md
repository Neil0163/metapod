k8 Core Concepts overview
- node and pod 
    Node
     - A simple server

    Pod 
    - smallest unit, creates a running environment layer ontop of the container so Kubernetesyou dont have to directley work with docker. 
    (Example)
    You could have a App pod and then a DB pod usually 1 app per pod. The pod has its own ip address which is internal and used to communicate with another pod. They are affemeral which means they can die easy, when this happens an app has usally crashed inside the pod or the node has ran out of resources.
    In this case the pod is replaced and assigned a new IP adress. 

- Service and Ingress 
    Service
    Service is a permanent IP address that can be attached to each pod, the lifecylces of service and pod are not connected so if a pod dies, you dont have to change the pod ip address when a new one is created as they are both linked.
    A external Service opens the communication from external sources, but we wouldnt want for example a db to be exposed to a external service, we can create and specify an internal service
    Service is also a load balancer.
    An internal service is a service that dosent accept external request - only components within the cluster can talk to it.

    Ingress
    This secures out external service, the call goes through and ingress which is then passed onto the service.
    (Example)
    Without ingress domain http://my-app:8080
    With ingress domain https://my-app.com

- ConfigMap and Secret 
    ConfigMap
    External config to your application, contains url of database, it connects to the pod so the pod gets the data the configmap contains, meaning if you want to change a database connection url you no longer have to build a new image push to repo etc. You just change the configmap.

    Secret
    Just like config map but this is used to stores secrets which is stored in base64 encoded 
    This connect to your pod just as configMap would 

 - Volumes
    Volume
    If a DB pod gets restarted the DB would be gone, 
    volumes attaches a pyhsical storage to your local machine or to a remote storage such as cloud storage this persits the database so you dont loose data.
    K8 dosnt manage the data its down to the user.

- Deployment
    Deployments are creating replicas of each node, you can define here whether to scale up or down, this is apart of a redundacy plan incase one pod goes down.
    Deployment is an abstraction of Pods 
    DB cant be replicated via deployment due to the database state 

- StatefulSet
    StatefulSets does the same job as deployment but ensures DB are synchd, deploying through StatefulSet can be difficult therefore its a common practice to host databse apps outside of the K8 cluster and deployments or stateless apps are in the k8 cluster.