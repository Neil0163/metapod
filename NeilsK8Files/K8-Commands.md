kubectl get nodes - gets a status of nodes 
kubectl version - shows Client and server version 
kubectl get pod - checks status of pod
kubectl get servicde - shows status of services 
kubectl create deployment <name> --image=<image> -     creates deployment
kubectl get deployment = shows deployment status 
kubectl get replicaset - shows replicaset 
kubectl edit deployment nging-depl - edits deployment
kubectl logs <Pod name> - shows running app logs
kubectl describe pod <pod name> -additional info
kubectl exec -it (interaxtive terminal)<pod name> --bin/bash - interactive terminal of container 
kubectl delete deployment <deployment name> - deletes deployment 
kubectl apply -f <file name>.yaml - uses config file to set up
kubectl port-forward <pod name> <App port:Local Port> 
kubectl get all - gets all componnents inside cluster 
kubectl apply -f <secret file name.yaml> - builds secret from file 
kubectl get secret - shows secret file 
kubectl apply -f <deployment file name.yaml> builds deployment from file 
kubectl describe service <service name> - shows service details
kubectl get pod -o wide - shows additional colums with ip address 
minikube service <name of service> - launches browser to see app 
kubectl create namespace <namespace name>  - creates namespace
kubectl apply -f <file name> --namespace=my-namespace - applies new namespace also
brew install kubectx - run kubens <namespace> kubectx is a tool 
helm search <keyword> - looks for a deployment chart 
helm install <chartname> installs files into keuberntes