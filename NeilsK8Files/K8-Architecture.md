K8 Architecture 
- Node Processes (worker node)
    Each node has multiple application pods with mulitple containers.
    3 processes must be installed on every worker node
    1 - Container run time needs to be installed on everynode such as docker the process that schedules this is kubelet, Kubelet intereacts with both the container and the node. Kubelet starts the pod with the container inside and assign resources from the node to the container. 
    2 - Services, services has been discussed above 
    3 - Kube proxy forwards request and ensures that communication works effeicntley with a low overhead.

- Master Processes (control worker Nodes)
    A Master Node must have 4 processes that run on each one
    1 - Api Server, it gets the inital request of any updates into the cluster, it also acts as a gate keeper for authentication, you have to talk to the API server in order send request to build other nodes or components, query status of deployments, it provides 1 entry point into the cluster which is good for security 

    Scheduler
    Api hands over to shecudlar to start the application pod on one of the worker nodes. 
    it looks at your request and works out how much resources it needs and then looks to your pod to schedule the new pod on that node, it decides which node the new pod will be shceduled. 

    Controller manager
    This detects when a pod has died, it detects cluster state changes, it makes a request to the scheduler to re-sched the pod which is then passed on to kublet 

    etcd
    This is the cluster brain, any changes to the clusters get saved and updated here. 
    Note that application data is not stored here. 

    K8 Cluster is usally made up of many master nodes.
    etcd works acorss these. 
    
    Example Cluster

    A cluster will have usually have:
    2 Master Nodes 
    3 Worker Nodes
    Master nodes have less resources as they dont demand as much resources as worker nodes do. As demand for resources increase you may add more master a node servers to your cluster to meet requirments. 
    To do this you would typically follow this process:
    1 - Get a new bare server 
    2 - install all the master/worker node processes
    3 - add it to the cluster 

    
