Example config file for K8 

- Their is 3 parts to a config file 

1) metadata 
2) specification 
3) status (automatically generated)
Use a yaml online validator to see syntax errors, these files are usually stored with your code 
or your onw git repo with config files. 




apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongo-deployment 
  lables:
    aop: mongodb
spec:
  replicas: 3
  selector:
    matchLabels:
      app: mongodb
  template:
    metadata:
      labels:
        app: mongodb
    spec:
      containers:
        - name: mongodb
          image: mongo        (check details on how to use on docker eg varibles, ports)
          ports:
            - containerPort: 27017
          env:
            - name: MONGO_INITDB_ROOT_USERNAME
              valueFrom:
                secretKeyRef:
                  name: mongodb-secret
                  key: mongo-root-password  

apiVersion: v1
kind: Service
metadata:
  name: mongodb-service
spec:
  selector:
    app: mongodb
  ports:
    - protocol: TCP
      port: 27017
      targetPort: 27017

