apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongo-express
  lables:
    app: mongo-express
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mongo-express
  template:
    metadata:
      labels:
        app: mongo-express
    spec:
      containers:
        - name: mongo-express
          image: mongo-express:latest
          ports:
            - containerPort: 8081
          env:
            - name: ME_CONFIG_MONGODB_ADMINUSERNAME
              valueFrom:
                secretKeyRef:
                  name: mongodb-secret
                  key: mongo-root-password  
            - name: ME_CONFIG_MONGODB_ADMINPASSOWRD
              valueFrom:
                secretKeyRef:
                  name: mongodb-secret
                  key: mongo-root-password  
            - name: ME_CONFIG_MONGODB_SERVER
              valueFrom:
                configMapKeyRef:
                  name: mongodb-configmap
                  key: database_url
---
(External service configeration)
apiVersion: v1
kind: Service
metadata:
  name: mongo-express 
spec:
  selector:
    app: mongo-express
  type: LoadBalancer << accepts external request by assinign an ip address 
  ports:
    - protocol: TCP
      port: 8081
      targetPort: 8081
      nodePort: 30000  << this is the port you will use in your browser note has to be between 30000 - 32767