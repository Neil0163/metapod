apiVersion: v1
kind: Secret
metadata:
    name: mongodb-secret
type: Opaque
data:
    mongo-root-username: dXNlcm5hbWU=  (echo -n 'username' | base64) <<< encodes username to base64
    mongo-root-password: cGFzc3dvcmQ=  (echo -n 'password' | base64) <<< encodes password to base64



To apply the secret ensure that both the deployment file for the secret and the secret file itself are in the same directory, then go to the terminal and run: 
kubectl apply -f <secret file name.yaml>

