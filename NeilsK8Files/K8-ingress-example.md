You need an implementation for ingress which is ingress controller

run minikube addons enable ingress-starts K8 nginx ingress 

now create and ingress rule 

apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
    name: neils-ingress
    namespace: neilsNameSpace
spec:
    rules:
    - host: neil.com (needs to be registered)
      http: 
        paths:
        -backend:
        serviceName: <service name>
        servicePort: <Port where service listens>

you need to map the ip address given to the domain in the etc/hosts files.